<?php

namespace Drupal\frontendpublishing\Plugin\frontendpublishing;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Describes the interface for state change plugin handlers.
 */
interface StateChangePluginInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Transition an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The entity to be transitioned.
   */
  public function transition(ContentEntityBase &$entity);

  /**
   * Publish an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The entity to be published.
   */
  public function publish(ContentEntityBase &$entity);

  /**
   * Unpublish an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The entity to be unpublished.
   */
  public function unpublish(ContentEntityBase &$entity);

  /**
   * Copy an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The entity to be copied.
   * @param \Drupal\Core\Entity\ContentEntityBase $clone
   *   The copied entity.
   */
  public function copy(ContentEntityBase &$entity, ContentEntityBase &$clone = NULL);

  /**
   * Generate an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The entity to be generated.
   */
  public function generate(ContentEntityBase &$entity);

  /**
   * Delete an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The entity to be deleted.
   */
  public function delete(ContentEntityBase &$entity);

}
