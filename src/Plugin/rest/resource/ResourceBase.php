<?php

namespace Drupal\frontendpublishing\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\frontendpublishing\Service\StateChange;
use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase as Base;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class ResourceBase extends Base {

  /**
   * The state change service.
   *
   * @var \Drupal\frontendpublishing\Service\StateChange
   */
  protected $stateChange = NULL;

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser = NULL;

  /**
   * Constructs a new UnpublishResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\frontendpublishing\Service\StateChange $state_change
   *   State changer service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    StateChange $state_change
    ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->stateChange = $state_change;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('frontendpublishing'),
      $container->get('current_user'),
      $container->get('frontendpublishing.state_change')
    );
  }

  /**
   * Returns the node.
   *
   * If the node is moderated, it will automatically fetch the newest revision.
   *
   * @param int $nid
   *   The node id.
   *
   * @return Drupal\node\Entity\Node|null
   *   The node or null if the node does not exist.
   */
  protected function getNewestNodeRevision($nid) {
    $entity = Node::load($nid);
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('content_moderation') && \Drupal::service('content_moderation.moderation_information')->isModeratedEntity($entity)) {
      $entityTypeManager = \Drupal::service('entity_type.manager');
      $storage = $entityTypeManager->getStorage('node');
      $entity = $storage->loadRevision($storage->getLatestRevisionId($nid));
    }
    return $entity;
  }

}
