<?php

namespace Drupal\frontendpublishing\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Provides a resource to copy a page and place the new entry in the menu tree.
 *
 * @RestResource(
 *   id = "frontendpublishing_copy",
 *   label = @Translation("Copy a page"),
 *   uri_paths = {
 *     "create" = "/frontendpublishing/copy"
 *   }
 * )
 */
class Copy extends Move {

  /**
   * Responds to POST requests.
   *
   * Copies a page and it's children under the new parent at the given position.
   * Rebuilds menu.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The response containing the id of the copy.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function post(Request $request) {
    $requestContent = json_decode($request->getContent(), TRUE, 512, JSON_THROW_ON_ERROR);
    $id = $requestContent['id'];
    $parentId = $requestContent['newParent'];
    $weight = $requestContent['weight'];
    $recursive = $requestContent['recursive'];
    $menu = $requestContent['menu'];

    /** @var \Drupal\node\Entity\Node $entity */
    $entity = $this->getNewestNodeRevision($id);
    if (empty($menu) || $menu == NULL) {
      $menu = 'main';
    }

    if ($entity == NULL) {
      throw new UnprocessableEntityHttpException('Entity not found.');
    }

    if (!$entity->access('create', \Drupal::currentUser())) {
      throw new AccessDeniedHttpException('You are not allowed to create a new node.');
    }

    $result = $this->clone($entity, $menu, $parentId);
    $clone = $result[0];
    $menuLink = $result[1];
    if ($menuLink != NULL) {
      $menuLink->weight->value = $weight * 10;
      $menuLink->save();
      $this->menuHelper::reorder($menuLink->parent->value, $menuLink, $menu);
      if ($recursive) {
        $root = $this->menuHelper::getMenuLink($entity, $menu);
        if ($root != NULL) {
          $tree = $this->menuHelper::getMenuTree($menu, $root->getPluginId());
          $parentId = str_replace('menu_link_content:', '', (string) $menuLink->getPluginId());
          $this->copyTree($tree, $clone, $menu, $parentId);
        }
      }
    }

    $url = $clone->toUrl('canonical', ['absolute' => TRUE])->toString(TRUE);
    return new ModifiedResourceResponse([
      'clone' => $clone->id(),
      'original' => $entity->id(),
    ], 201, ['Location' => $url->getGeneratedUrl()]);
  }

  /**
   * {@inheritdoc}
   */
  protected function copyTree($tree, $parentClone, $menu, $parentId) {
    foreach ($tree as $entry) {
      $nid = $entry->link->getRouteParameters()['node'];
      if ($nid != $parentClone->id()) {
        $entity = $this->getNewestNodeRevision($nid);
        $result = $this->clone($entity, $menu, $parentId);
        $clone = $result[0];
        $subParentId = str_replace('menu_link_content:', '', (string) $entry->link->getPluginId());
        $this->copyTree($entry->subtree, $clone, $menu, $subParentId);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function clone($entity, $menu, $parentId) {
    $clone = $this->stateChange->copy($entity);
    $menuLink = $this->menuHelper::getMenuLink($clone, $menu);
    if ($menuLink == NULL) {
      $menuLink = $this->menuHelper::createMenuLink($clone, $clone->title->value, $parentId, $menu);
    }
    foreach ($entity->getTranslationLanguages(FALSE) as $language) {
      if ($clone->hasTranslation($language->getId()) && !$menuLink->hasTranslation($language->getId())) {
        $menuLink->addTranslation($language->getId(), ['title' => $clone->getTranslation($language->getId())->title->value])->save();
      }
    }
    if ($parentId != -1) {
      $parentLink = $this->menuHelper::loadMenuLink($parentId);
      $menuLink->parent->value = $parentLink->getPluginId();
    }
    if ($menuLink != NULL) {
      $menuLink->save();
    }
    return [$clone, $menuLink];
  }

}
