<?php

namespace Drupal\frontendpublishing\Plugin\rest\resource;

use Drupal\frontendpublishing\Service\MenuHelper;
use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a resource to move a page in the menu tree.
 *
 * @RestResource(
 *   id = "frontendpublishing_move",
 *   label = @Translation("Move a page"),
 *   uri_paths = {
 *     "canonical" = "/frontendpublishing/move"
 *   }
 * )
 */
class Move extends ResourceBase {

  /**
   * The menu helper service.
   *
   * @var \Drupal\frontendpublishing\Service\MenuHelper
   */
  protected $menuHelper = NULL;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setMenuHelper($container->get('frontendpublishing.menu_helper'));
    return $instance;
  }

  /**
   * Set the menu helper.
   */
  public function setMenuHelper(MenuHelper $menu_helper) {
    $this->menuHelper = $menu_helper;
  }

  /**
   * Moves a page under the new parent at the given position. Rebuilds menu.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\rest\ModifiedResourceResponseModifiedResourceResponse
   *   The response containing a list of bundle names.
   */
  public function patch(Request $request) {
    $requestContent = json_decode($request->getContent(), TRUE, 512, JSON_THROW_ON_ERROR);
    $pluginId = $requestContent['id'];
    $parentId = $requestContent['newParent'];
    $position = $requestContent['weight'];
    $menu = $requestContent['menu'];

    $response = [];
    if (empty($menu) || $menu == NULL) {
      $menu = 'main';
    }

    $menuLink = $this->menuHelper::loadMenuLink($pluginId);
    if ($menuLink != NULL) {
      $oldParent = $menuLink->parent->value;
      if (!empty($parentId)) {
        $parentMenuLink = $this->menuHelper::loadMenuLink($parentId);
        if ($parentMenuLink != NULL) {
          $menuLink->parent->value = $parentMenuLink->getPluginId();
        }
        else {
          $menuLink->parent->value = '';
        }
      }
      else {
        $menuLink->parent->value = '';
      }

      if ($menuLink->parent->value != $oldParent) {
        $this->menuHelper::reorder($oldParent, NULL, $menu);
      }
      $this->menuHelper::saveAndReorder($menuLink, $position, $menu);
      $this->menuHelper::clearCache($menu);

      $response = $requestContent;
    }
    else {
      $response = ['error' => 'Could not find menu link ' . $pluginId . ' in menu ' . $menu];
    }
    return new ModifiedResourceResponse($response);
  }

}
