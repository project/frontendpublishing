<?php

namespace Drupal\frontendpublishing\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Provides a resource to get the avaiable transitions.
 *
 * @RestResource(
 *   id = "frontendpublishing_transitions",
 *   label = @Translation("Get avaiable transitions"),
 *   uri_paths = {
 *     "canonical" = "/frontendpublishing/transitions/{nid}"
 *   }
 * )
 */
class Transitions extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * @param int $nid
   *   The entity id.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing a list of bundle names.
   */
  public function get($nid = NULL) {
    $entity = $this->getNewestNodeRevision($nid);
    if ($entity == NULL) {
      throw new UnprocessableEntityHttpException('Entity not found.');
    }
    $list = [];
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('content_moderation') && \Drupal::service('content_moderation.moderation_information')->isModeratedEntity($entity)) {
      $transitions = \Drupal::service('content_moderation.state_transition_validation')->getValidTransitions($entity, $this->currentUser);
      foreach ($transitions as $transition) {
        $list[] = [
          'id' => $transition->to()->id(),
          'label' => $transition->to()->label(),
          'published' => $transition->to()->isPublishedState(),
          'default' => $transition->to()->isDefaultRevisionState(),
        ];
      }
    }
    $response = new ResourceResponse($list);
    $response->addCacheableDependency(['cache' => ['max-age' => 0]]);
    return $response;
  }

  /**
   * Responds to PATCH requests.
   *
   * @param int $nid
   *   The entity id.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing a list of bundle names.
   */
  public function patch($nid, Request $request) {
    $requestContent = json_decode($request->getContent(), TRUE, 512, JSON_THROW_ON_ERROR);
    $language = $requestContent['language'];
    $message = $requestContent['message'];
    $state = $requestContent['state'];

    $entity = $this->getNewestNodeRevision($nid);
    if ($entity == NULL) {
      throw new UnprocessableEntityHttpException('Entity not found.');
    }

    if (!$entity->access('update', $this->currentUser)) {
      throw new AccessDeniedHttpException('You are not allowed to edit this node.');
    }

    if ($entity->hasTranslation($language)) {
      $entity = $entity->getTranslation($language);
      $results = $this->stateChange->transition($entity, $message, $state);
    }
    else {
      throw new UnprocessableEntityHttpException('The given language is not available on the entity.');
    }
    return new ModifiedResourceResponse($results);
  }

}
