<?php

namespace Drupal\frontendpublishing\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Provides a resource to publish a node.
 *
 * @RestResource(
 *   id = "frontendpublishing_publish",
 *   label = @Translation("Publish a node"),
 *   uri_paths = {
 *     "canonical" = "/frontendpublishing/publish"
 *   }
 * )
 */
class Publish extends ResourceBase {

  /**
   * Publish a page and all rows, components and contents within.
   *
   * Returns a tree array of all published node and revision ids.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The published node and revision ids.
   */
  public function patch(Request $request) {
    $requestContent = json_decode($request->getContent(), TRUE, 512, JSON_THROW_ON_ERROR);
    $id = $requestContent['id'];
    $language = $requestContent['language'];
    $message = $requestContent['message'];

    $entity = $this->getNewestNodeRevision($id);
    if ($entity == NULL) {
      throw new UnprocessableEntityHttpException('Entity not found.');
    }

    if (!$entity->access('update', $this->currentUser)) {
      throw new AccessDeniedHttpException('You are not allowed to edit this node.');
    }

    if ($entity->hasTranslation($language)) {
      $entity = $entity->getTranslation($language);
      $results = $this->stateChange->publish($entity, $message);
    }
    else {
      throw new UnprocessableEntityHttpException('The given language is not available on the entity.');
    }
    return new ModifiedResourceResponse($results);

  }

}
