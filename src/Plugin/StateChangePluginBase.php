<?php

namespace Drupal\frontendpublishing\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\frontendpublishing\Plugin\frontendpublishing\StateChangePluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Public view renderer.
 *
 * Renders a node and its children in public (not editable) form.
 */
abstract class StateChangePluginBase extends PluginBase implements StateChangePluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition
      );
  }

  /**
   * {@inheritdoc}
   */
  public function publish(ContentEntityBase &$entity) {

  }

  /**
   * {@inheritdoc}
   */
  public function transition(ContentEntityBase &$entity) {

  }

  /**
   * {@inheritdoc}
   */
  public function unpublish(ContentEntityBase &$entity) {

  }

  /**
   * {@inheritdoc}
   */
  public function generate(ContentEntityBase &$entity) {

  }

  /**
   * {@inheritdoc}
   */
  public function delete(ContentEntityBase &$entity) {

  }

  /**
   * {@inheritdoc}
   */
  public function copy(ContentEntityBase &$entity, ContentEntityBase &$clone = NULL) {

  }

}
