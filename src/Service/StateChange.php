<?php

namespace Drupal\frontendpublishing\Service;

use Drupal\content_moderation\ModerationInformation;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * This service serves as an interface to trigger state changes.
 */
class StateChange {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The module handler service.
   *
   * @var \Drupal\frontendpublishing\Service\StateChangePluginManager
   */
  protected $stateChangePluginManager;

  /**
   * The moderation information service.
   *
   * @var \Drupal\content_moderation\ModerationInformation
   */
  protected $moderationInformation;

  /**
   * Constructs.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\frontendpublishing\Service\StateChangePluginManager $state_change_plugin_manager
   *   The module handler service.
   */
  public function __construct(ModuleHandlerInterface $module_handler, StateChangePluginManager $state_change_plugin_manager) {
    $this->moduleHandler = $module_handler;
    $this->stateChangePluginManager = $state_change_plugin_manager;
  }

  /**
   * Set moderation information service, if available.
   *
   * @param \Drupal\content_moderation\ModerationInformation $moderationInformation
   *   The moderation information service.
   */
  public function setModerationInformationService(ModerationInformation $moderationInformation) {
    if ($this->moduleHandler->moduleExists('content_moderation')) {
      $this->moderationInformation = $moderationInformation;
    }
  }

  /**
   * Publish a node without workflow.
   *
   * Calls the publish method of all state change handler.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being published.
   * @param string $message
   *   The revision log message.
   */
  public function publish(ContentEntityInterface &$entity, $message) {
    if ($entity instanceof RevisionLogInterface) {
      $entity->setRevisionLogMessage($message);
    }
    $handlers = $this->stateChangePluginManager->getHandlers();
    foreach ($handlers as $handler) {
      $handler->publish($entity);
    }
  }

  /**
   * Move a node to another state.
   *
   * Calls the transition method of all state change handler
   * or the publish or unpublish method if no workflow exists.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being moved to another state.
   * @param string $message
   *   The revision message.
   * @param string $state
   *   The target state.
   */
  public function transition(ContentEntityInterface &$entity, $message, $state) {
    if ($entity instanceof RevisionLogInterface) {
      $entity->setRevisionLogMessage($message);
    }
    if (isset($this->moderationInformation) && $this->moderationInformation->isModeratedEntity($entity)) {
      $entity->moderation_state->value = $state;
      $entity->setNewRevision();
      $handlers = $this->stateChangePluginManager->getHandlers();
      foreach ($handlers as $handler) {
        $handler->transition($entity);
      }
    }
    else {
      if ($state == 'publish') {
        $this->publish($entity, $message);
      }
      elseif ($state == 'unpublish') {
        $this->unpublish($entity, $message);
      }
    }
  }

  /**
   * Unpublish a node without workflow.
   *
   * Calls the publish method of all state change handler.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being unpublished.
   * @param string $message
   *   The revision log message.
   */
  public function unpublish(ContentEntityInterface &$entity, $message) {
    if ($entity instanceof RevisionLogInterface) {
      $entity->setRevisionLogMessage($message);
    }
    $handlers = $this->stateChangePluginManager->getHandlers();
    foreach ($handlers as $handler) {
      $handler->unpublish($entity);
    }
  }

  /**
   * Clones a entity and returns the duplicate entity.
   *
   * Clones the base language, creates translations and
   * passes each language version to the registered handlers.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to clone.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The cloned entity in the default translation.
   */
  public function copy(ContentEntityInterface &$entity) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $clone */
    $clone = NULL;
    $handlers = $this->stateChangePluginManager->getHandlers();
    foreach ($handlers as $handler) {
      $handler->copy($entity, $clone);
    }
    foreach ($clone->getTranslationLanguages(TRUE) as $language) {
      $clone->getTranslation($language->getId())->save();
    }
    $clone = $clone->getUntranslated();
    return $clone;
  }

}
