<?php

namespace Drupal\frontendpublishing\Service;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\Entity\Node;

/**
 * Static menu helper.
 *
 * Provides static methods for working with menus.
 */
class MenuHelper {

  /**
   * Create a menu link for the given node id.
   *
   * @param \Drupal\node\Entity\Node $entity
   *   The node.
   * @param string $title
   *   The menu link title.
   * @param string $parentId
   *   ID of the parent item.
   * @param string $menu
   *   The menu name.
   */
  public static function createMenuLink(Node $entity, string $title, string $parentId, string $menu = 'main') {
    $linkDef = [
      'title' => $title,
      'link' => [
        'uri' => 'entity:node/' . $entity->id(),
      ],
      'menu_name' => $menu,
      'weight' => 1000,
      'bundle' => 'menu_link_content',
      'expanded' => TRUE,
    ];
    if (!empty($parentId)) {
      $parentLinkItem = self::loadMenuLink($parentId);
      if ($parentLinkItem) {
        $linkDef['parent'] = $parentLinkItem->getPluginId();
        $linkDef['enabled'] = $parentLinkItem->isEnabled();
        if ($menu == '') {
          $linkDef['menu_name'] = $parentLinkItem->getMenuName();
        }
      }
      else {
        if ($menu == '') {
          $linkDef['menu_name'] = 'main';
        }
      }
    }
    else {
      if ($menu == '') {
        $linkDef['menu_name'] = 'main';
      }
    }
    $menuLink = MenuLinkContent::create($linkDef);
    $menuLink->save();
    return $menuLink;
  }

  /**
   * Load a menu link by its uuid.
   *
   * @param string $uuid
   *   The UUID.
   */
  public static function loadMenuLink(string $uuid) {
    $menuLink = NULL;
    $uuid = str_replace('menu_link_content:', '', $uuid);
    if (!empty($uuid)) {
      $query = \Drupal::entityQuery('menu_link_content')
        ->accessCheck()
        ->condition('uuid', $uuid, 'LIKE');
      $result = $query->execute();
      $menuLinkId = (!empty($result)) ? reset($result) : FALSE;

      if ($menuLinkId) {
        $menuLink = MenuLinkContent::load($menuLinkId);
      }
    }

    return $menuLink;
  }

  /**
   * Get a menu link content entity by content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $menu
   *   The menu name.
   *
   * @return \Drupal\menu_link_content\Entity\MenuLinkContent
   *   The menu link for the given node.
   */
  public static function getMenuLink(ContentEntityInterface $entity, string $menu = 'main') {
    $menuLink = NULL;
    $query = \Drupal::entityQuery('menu_link_content')
      ->accessCheck()
      ->condition('link.uri', '%' . $entity->getEntityTypeId() . '/' . $entity->id(), 'LIKE');
    if ($menu != '') {
      $query->condition('menu_name', $menu);
    }
    $result = $query->execute();
    $menuLinkId = (!empty($result)) ? reset($result) : FALSE;

    if ($menuLinkId) {
      $menuLink = MenuLinkContent::load($menuLinkId);
    }
    return $menuLink;
  }

  /**
   * Set the parent of the given menu link using an entity.
   *
   * @param \Drupal\menu_link_content\Entity\MenuLinkContent $menuLink
   *   The menu link.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $menu
   *   The menu name.
   */
  public static function setParent(MenuLinkContent $menuLink, ContentEntityInterface $entity, string $menu = 'main') {
    $parentLinkItem = NULL;
    $parentMenuLink = self::getMenuLink($entity, $menu);
    if ($parentLinkItem) {
      $menuLink->parent->value = $parentMenuLink->getPluginId();
    }
    else {
      $menuLink->parent->value = '';
    }
    $menuLink->save();
  }

  /**
   * Save a menu link and reorder the level.
   *
   * Takes the the current menulink content and it's position on the level.
   *
   * @param \Drupal\menu_link_content\Entity\MenuLinkContent $current
   *   The current link item.
   * @param int $position
   *   The new position.
   * @param string $menu
   *   The menu name.
   */
  public static function saveAndReorder(MenuLinkContent $current, int $position, string $menu = 'main') {
    $current->weight->value = $position * 10;
    $current->save();
    $parentId = $current->parent->value;
    self::reorder($parentId, $current, $menu);
  }

  /**
   * Reorder a menu level.
   *
   * Reorders the menu level with $parentId as parent.
   * Skips the entry given with $skip.
   *
   * @param string $parentId
   *   The ID of the parent menu link.
   * @param \Drupal\menu_link_content\Entity\MenuLinkContent $skip
   *   The menu item to be skipped.
   * @param string $menu
   *   The menu name.
   */
  public static function reorder(string $parentId = NULL, MenuLinkContent $skip = NULL, string $menu = 'main') {
    $query = \Drupal::entityQuery('menu_link_content')
      ->accessCheck()
      ->condition('menu_name', $menu);

    if (!empty($parentId)) {
      $query->condition('parent', $parentId);
    }
    else {
      $query->condition('parent', '', 'IS NULL');
    }
    if ($skip != NULL) {
      $query->condition('id', $skip->id(), "<>");
    }
    $query->sort('weight');
    $menuLinkIds = $query->execute();

    $menuLinks = MenuLinkContent::loadMultiple($menuLinkIds);
    $currentWeight = 10;
    foreach ($menuLinks as $menuLink) {
      if ($skip != NULL && $currentWeight == $skip->weight->value) {
        $currentWeight += 10;
      }
      $menuLink->weight->value = $currentWeight;
      $menuLink->save();
      $currentWeight += 10;
    }
  }

  /**
   * Get the root menu link item for a content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The ID of the content entity.
   * @param string $menu
   *   The menu name.
   */
  public static function getRoot(ContentEntityInterface $entity, string $menu = 'main') {
    $menuLink = self::getMenuLink($entity, $menu);
    if ($menuLink) {
      $i = 0;
      while ($menuLink->parent->value != '' && $i < 10) {
        $menuLink = self::loadMenuLink($menuLink->parent->value);
        $i++;
      }
    }
    return $menuLink;
  }

  /**
   * Returns the menu tree.
   *
   * Returns the ordered, full menu tree, starting at the given root
   * (or at the top, if omitted).
   *
   * @param string $menu
   *   The menu name.
   * @param int $root
   *   The root plugin id.
   *
   * @return MenuLinkTreeElement[]
   *   The tree for the given menu
   */
  public static function getMenuTree(string $menu = 'main', $root = '') {
    $menuLinkTree = \Drupal::menuTree();
    $parameters = new MenuTreeParameters();
    $parameters->setMinDepth(0)->setMaxDepth(99);
    if (!empty($root)) {
      $parameters->setRoot($root);
    }
    $manipulators = [['callable' => 'menu.default_tree_manipulators:generateIndexAndSort']];
    $tree = $menuLinkTree->load($menu, $parameters);
    $tree = $menuLinkTree->transform($tree, $manipulators);
    return $tree;
  }

  /**
   * Clears the menu cache.
   *
   * @param string $menu
   *   The menu name.
   */
  public static function clearCache(string $menu = 'main') {
    Cache::invalidateTags(['config:system.menu.' . $menu]);
    \Drupal::service('plugin.manager.menu.link')->rebuild();
  }

}
