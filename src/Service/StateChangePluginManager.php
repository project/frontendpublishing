<?php

namespace Drupal\frontendpublishing\Service;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\frontendpublishing\Annotation\StateChangeHandler;
use Drupal\frontendpublishing\Plugin\frontendpublishing\StateChangePluginInterface;

/**
 * State change plugin manager.
 *
 * This class provides static functions for retrieving state change handlers.
 *
 * @see plugin_api
 */
class StateChangePluginManager extends DefaultPluginManager implements FallbackPluginManagerInterface {

  /**
   * Constructs a StateChangePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
          'Plugin/frontendpublishing/StateChange',
          $namespaces,
          $module_handler,
          StateChangePluginInterface::class,
          StateChangeHandler::class
      );
    $this->alterInfo('frontendpublishing_statechange_info');
    $this->setCacheBackend($cache_backend, 'frontendpublishing_statechange_info_plugins');
  }

  /**
   * Return all handlers, order by weight descending.
   *
   * @return Drupal\frontendpublishing\Plugin\frontendpublishing\StateChangePluginInterface[]
   *   The handlers.
   */
  public function getHandlers() {
    $handlers = [];
    $definitions = $this->getDefinitions();
    uasort($definitions, [SortArray::class, 'sortByWeightElement']);
    foreach ($definitions as $id => $definition) {
      $handlers[] = $this->getFactory()->createInstance($id, $definition);
    }
    return $handlers;
  }

  /**
   * {@inheritDoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'standard';
  }

}
