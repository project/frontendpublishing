(function ($, Drupal) {
  var initialized;

  Drupal.behaviors.frontendpublishing = {
    attach: function (context, settings) {
      if (!initialized) {
        Drupal.frontendpublishing = new Frontend_Publisher_Actions();
        initialized = true;
        $('body').append($('#frontend-publishing-publish-modal'));
        $('body').append($('#frontend-publishing-transition-modal'));
        $('body').append($('#frontend-publishing-unpublish-modal'));
      }
    }
  };
})(jQuery, Drupal);
