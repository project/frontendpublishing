/**
 *
 *
 *
 */
class Frontend_Publisher_Actions {

  /**
   * Initialize
   */
  constructor(settings) {
    this.settings = settings;
  }

  getTransitions(nodeId) {
    return Drupal.restconsumer
      .get('/frontendpublishing/transitions/' + nodeId, false, false).done()
  }

  showTransitionDialog(nodeId, lang, publish) {
    this.getTransitions(nodeId).done(function (response) {
      try {
        jQuery('#frontend-publishing-publish-modal').dialog("close");
      } catch (e) { }
      try {
        jQuery('#frontend-publishing-unpublish-modal').dialog("close");
      } catch (e) { }
      try {
        jQuery('#frontend-publishing-transition-modal').dialog("close");
      } catch (e) { }
      var dialog = jQuery('#frontend-publishing-publish-modal');
      var confirmButtonLabel = Drupal.t('Publish content');
      var cancelButtonLabel = Drupal.t('Cancel');
      if (response.length > 0) {
        var dialog = jQuery('#frontend-publishing-transition-modal');
        var confirmButtonLabel = Drupal.t('Transition content');
      } else if (!publish) {
        var dialog = jQuery('#frontend-publishing-unpublish-modal');
        var confirmButtonLabel = Drupal.t('Unpublish content');
      }
      var transitionSelection = dialog.find('#transition_selection')
      var transitionSelectionTitle = dialog.find('#transition_selection_title');
      transitionSelection.empty();
      if (response.length > 0) {
        for (var x in response) {
          transitionSelection.append('<option value="' + response[x]['id'] + '">' + response[x]['label'] + '</option>');
        }
        transitionSelection.show();
        transitionSelectionTitle.show();
      } else {
        if (publish) {
          transitionSelection.append('<option selected="selected" value="publish">Publish</option>');
        } else {
          transitionSelection.append('<option selected="selected" value="unpublish">Unpublish</option>');
        }
        transitionSelectionTitle.hide();
        transitionSelection.hide();
      }

      var dialogButtons = {};
      dialogButtons[confirmButtonLabel] = (function (action) {
        return function () {
          jQuery(this).dialog("close");
          var message = dialog.find("#revision_message").val();
          var state = transitionSelection.val();
          Drupal.frontendpublishing.transitionNode(nodeId, lang, state, message);
        }
      })(jQuery(this));
      dialogButtons[cancelButtonLabel] = function () { jQuery(this).dialog("close"); };

      dialog.find('#revision_message').val("");
      dialog.dialog({
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: dialogButtons
      });

    });
  }

  transitionNode(nodeId, lang, state, message) {
    jQuery(window).trigger("frontendpublishing_transition_start", [nodeId, lang, state, message])
    Drupal.restconsumer
      .patch('/frontendpublishing/transitions/' + nodeId, { state: state, language: lang, message: message }, false)
      .done(
        (function (nodeId, lang, state, message) {
          return function (e) {
            jQuery(window).trigger("frontendpublishing_transition_done", [nodeId, lang, state, message])
          }
        })(nodeId, lang, state, message)
      )
      .fail(
        (function (nodeId, lang, state, message) {
          return function (e) {
            jQuery(window).trigger("frontendpublishing_transition_fail", [nodeId, lang, state, message])
          }
        })(nodeId, lang, state, message)
      );
  }
}
